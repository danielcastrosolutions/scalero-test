from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from scalero_test.components.permissions import IsOwnerOrReadOnly
from scalero_test.movies import models
from scalero_test.movies import serializers


class MovieViewSet(viewsets.ModelViewSet):
    queryset = models.Movie.objects.all()
    serializer_class = serializers.MovieSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def __init__(self, *args, **kwargs):
        super(MovieViewSet, self).__init__(*args, **kwargs)
        self.serializer_action_classes = {
            # 'list': serializers.MovieListSerializer,
            'create': serializers.MovieCreateSerializer,
        }

    def get_serializer_class(self, *args, **kwargs):
        """Instantiate the list of serializers per action from class attribute (must be defined)."""
        kwargs['partial'] = True
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super(MovieViewSet, self).get_serializer_class()

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = models.Review.objects.all()
    serializer_class = serializers.ReviewSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)
