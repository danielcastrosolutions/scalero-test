from rest_framework import serializers

from scalero_test.movies import models


class ReviewSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField()
    class Meta:
        model = models.Review
        fields = '__all__'
        read_only_fields = ('created_by',)

    def get_created_by(self, obj):
        return obj.created_by.get_full_name()

class MovieReviewSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField()
    class Meta:
        model = models.Review
        fields = ('id', 'created_by', 'created_at', 'updated_at', 'content', 'positive',)

    def get_created_by(self, obj):
        return obj.created_by.get_full_name()


class MovieListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Movie
        fields = '__all__'


class MovieSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField()
    reviews = MovieReviewSerializer(many=True)

    class Meta:
        model = models.Movie
        fields = (
            'id',
            'name',
            'description',
            'year',
            'likes',
            'dislikes',
            'created_at',
            'updated_at',
            'created_by',
            'reviews',
        )

    def get_created_by(self, obj):
        return obj.created_by.get_full_name()


class MovieCreateSerializer(serializers.ModelSerializer):
    created_by = serializers.SerializerMethodField()
    class Meta:
        model = models.Movie
        fields = ('name', 'description', 'year', 'created_by', 'id',)
        read_only_fields = ('created_by', 'id',)

    def get_created_by(self, obj):
        return obj.created_by.get_full_name()
