from django.db import models


class Movie(models.Model):
    name = models.TextField()
    description = models.TextField()
    year = models.IntegerField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    created_by = models.ForeignKey('users.User', on_delete=models.CASCADE)

    @property
    def likes(self):
        return self.reviews.filter(positive=True).count()

    @property
    def dislikes(self):
        return self.reviews.filter(positive=False).count()

    def __str__(self):
        return self.name


class Review(models.Model):
    content = models.TextField()
    positive = models.BooleanField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    created_by = models.ForeignKey('users.User', on_delete=models.CASCADE)
    movie = models.ForeignKey('movies.Movie', on_delete=models.CASCADE, related_name='reviews')
