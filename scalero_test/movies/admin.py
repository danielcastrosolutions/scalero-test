from django.contrib import admin

from scalero_test.movies import models

# Register your models here.
admin.site.register(models.Movie)
admin.site.register(models.Review)
