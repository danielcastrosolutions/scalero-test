from django.urls import re_path, include
from rest_framework import routers

from scalero_test.users import api


router = routers.DefaultRouter()
router.register('users', api.UserReport)
router.register('companies', api.CompanyViewSet)

urlpatterns = [
    re_path("^", include(router.urls))
]