from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Users with the group "admin" have full access.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        admin = request.user.groups.filter(name="admin").exists()
        if request.method in permissions.SAFE_METHODS or admin:
            return True

        # Instance must have an attribute named `owner`.
        return obj.created_by == request.user
