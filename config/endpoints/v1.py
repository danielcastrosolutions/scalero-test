from django.urls import re_path, include
from rest_framework import routers

from scalero_test.movies import api

router = routers.DefaultRouter()

router.register('movies', api.MovieViewSet)
router.register('reviews', api.ReviewViewSet)

urlpatterns = [
    re_path("^", include(router.urls))
]
