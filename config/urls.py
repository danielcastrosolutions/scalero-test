# TODO: Update Docstring
"""Scalero Test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.generic import TemplateView

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from scalero_test.users.api import LoginAPI, UserAPI


if settings.DEBUG:
    yasg_base_url = 'http://localhost:8000'
else:
    yasg_base_url = f"https://{settings.ALLOWED_HOSTS[0]}"

schema_view = get_schema_view(
    openapi.Info(
        title="Movies API",
        default_version='v1',
        description="Scalero BackEnd Sample project",
        # terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@danielcastro.solutions"),
        # license=openapi.License(name="BSD License"),
    ),
    url=f"{yasg_base_url}/api/v1/",
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    url("^api/auth/user/$", UserAPI.as_view()),
    url("^api/auth/login/$", LoginAPI.as_view(), name='login'),
    path('api/auth/', include('knox.urls')),
    path('api/v1/', include('config.endpoints.v1')),
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    re_path(r'^(?:(?!api/)|.*Y).*$', TemplateView.as_view(template_name='index.html')),
]
