from .base import *

# GENERAL
# ------------------------------------------------------------------------------
SECRET_KEY = env('DJANGO_SECRET_KEY', default='9niqoZOGsnWoMUqdfDWe4dKPZESwlGB85jvd9hSDeN0MrJObwRgI0jP6k47G6mdK')

DEBUG = False

ALLOWED_HOSTS = ['scalero.danielcastro.solutions']

# django_admin_env_notice
# ------------------------------------------------------------------------------
ENVIRONMENT_NAME = "Production server"
ENVIRONMENT_COLOR = "#FF2222"

# Static
# ------------------------------------------------------------------------------
STATICFILES_DIRS += [
    'frontend/build/static',
    'frontend/build',
]

# Your stuff...
# ------------------------------------------------------------------------------
