#!/bin/sh

set -o errexit
set -o nounset
set -o xtrace


npm install
npm run start
