# Scalero Backend Developer Test, Movies edition

This project was built based on Scalero's sample project for the backend developer position. The project's description
can be read [here][project-description].

The completed project can be found working live [here][project-prod]. Here you can also access the 
[Django Admin][admin-prod], [explorable API][apiroot-prod], [swagger][swagger-prod] and [redoc][redoc-prod].

The following users are available:
  
    user1
    user2
    admin_user

All use the same password:

    supersecret

In order to view the explorable API and interact with the API on the swagger endpoint you need to log in to the 
admin first in order to get an active session.

**IMPORTANT: Make sure you don't log in to the frontend, and the admin within the same session as this will cause
session errors as you will have one active for Django and another for the React frontend. I recommend using private mode 
in a browser for one or the other in order to avoid this issue.**

---
# Authentication
In order to use the API, I'm giving you the following token for admin_user which you have to send on every request in 
case you want to skip the creation of the session tokens. I forced this token's expiration date to next year so that you 
can keep using it without worrying about sessions: 

    e97dac6540a22bb87df6906a5e9762a5d97936241eb6002e8252009af359195f

Example request using CURL: 

    curl -H "Authorization: Token e97dac6540a22bb87df6906a5e9762a5d97936241eb6002e8252009af359195f"  https://scalero.danielcastro.solutions/api/v1/

You can also create more tokens through swagger (or from wherever you prefer), sending a username and password on the /auth/login/ section in the swagger endpoint.

----
# Requirements

* Docker
* docker-compose (the latest Docker version includes the docker compose command, but I still use the docker-compose
  python package)
* npm in case you want to run the frontend code locally without docker-copose

---
# Running the project's backend

Build the Python image using using `docker-compose`

    docker-compose -f local.yml build

As soon as the image is done building you can run the project with the following command:

    docker-compose -f local.yml up

Once everything is running (you can make sure by visiting the [Django Admin][admin]) create a superuser:

    docker-compose -f local.yml run --rm django python manage.py createsuperuser

With the user created, log in to the [Django Admin][admin] section which will give you an active session with which you'll be able
to visit the [explorable API root][api-root].

You can find additional testing/documentation with [swagger] and [redoc]

---

# Running the project's frontend

There are two different ways to run this project, with local npm commands or through docker-compose.

Run it locally:

    cd frontend/
    npm install
    npm run start

Run it with docker-compose:

    docker-compose -f node.yml run --rm /build.sh
    docker-compose -f node.yml up

I recommend running the node service locally as it runs faster and installing the packages using docker-compose if you
run into any installation errors. You could also just run everything through docker-compose and avoid all project
requirements.

You can visit the [frontend url][frontend] where you can log in with any active user in order to create and explore movies,
as well as edit and review them.

---
# Relevant endpoints and URLs

[http://localhost:8000/admin/][admin]

[http://localhost:8000/api/v1/][api-root]

[http://localhost:8000/swagger/][swagger]

[http://localhost:8000/redoc/][redoc]

[http://localhost:8000/][frontend]

[https://scalero.danielcastro.solutions/][project-prod]

[https://scalero.danielcastro.solutions/admin/][admin-prod]

[https://scalero.danielcastro.solutions/api/v1/][apiroot-prod]

[https://scalero.danielcastro.solutions/swagger/][swagger-prod]

[https://scalero.danielcastro.solutions/redoc/][redoc-prod]


[admin]: http//:localhost:8000/admin/
[api-root]: http://localhost:8000/api/v1/
[frontend]: http://localhost:8000/
[swagger]: http://localhost:8000/swagger/
[redoc]: http://localhost:8000/redoc/
[project-description]: https://www.notion.so/BackEnd-Sample-project-76b08a70b3c245ac946afe19ee376733
[project-prod]: https://scalero.danielcastro.solutions/
[admin-prod]: https://scalero.danielcastro.solutions/admin/
[apiroot-prod]: https://scalero.danielcastro.solutions/api/v1/
[swagger-prod]: https://scalero.danielcastro.solutions/swagger/
[redoc-prod]: https://scalero.danielcastro.solutions/redoc/