import React, {Component} from "react";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";

import "bootstrap/dist/css/bootstrap.min.css";
import "./shards-dashboard/styles/shards-dashboards.1.1.0.min.css";
import projectApp from "./reducers";
import {Provider} from "react-redux";
import Routes from "./components/Routes";
import {loadUser} from "./actions/auth";

import "./App.css"

let store = createStore(projectApp, applyMiddleware(thunk));


class App extends Component {
    componentDidMount() {
        store.dispatch(loadUser());
    }

    render () {
        return (
            <Provider store={store}>
                <Routes />
            </Provider>
        )
    }
}

export default App;
