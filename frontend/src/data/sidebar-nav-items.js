export default function () {
    return [
        {
            title: "Home",
            to: "/movies",
            htmlBefore: '<i class="material-icons">home</i>',
            htmlAfter: "",
            groups: [],
        },
    ];
}
