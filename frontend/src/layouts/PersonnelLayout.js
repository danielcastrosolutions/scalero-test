import React from "react";
import {Col, Container, Row} from "shards-react";

const PersonnelLayout = ({ children }) => (
  <Container>
    <Row>
      <Col
        className="mx-auto"
        lg={{ size: 8}}
        md={{ size: 8}}
        sm="12"
        tag="main"
      >
        {children}
      </Col>
    </Row>
  </Container>
);


export default PersonnelLayout;
