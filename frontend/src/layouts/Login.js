import React from "react";
import {Col, Container, Row} from "shards-react";

const LoginLayout = ({ children }) => (
  <Container fluid>
    <Row>
      <Col
        className="mx-auto"
        lg={{ size: 4}}
        md={{ size: 6}}
        sm="12"
        tag="main"
      >
        {children}
      </Col>
    </Row>
  </Container>
);


export default LoginLayout;
