import React from "react";
import {Route, Redirect} from "react-router-dom";
import {connect} from "react-redux";

const GroupRoute = ({component: Component, auth, groups, ...rest}) => (
    <Route
        {...rest}
        render={props => {
            if (auth.isLoading) {
                return <h2>Loading...</h2>;
            } else if (auth.isAuthenticated) {
                let intersections = groups.filter(e => auth.user.groups.indexOf(e) !== -1);

                if (groups.length === 0 || intersections.length > 0) {
                    return <Component {...props} />;
                } else {
                    return (
                        <Redirect
                            to={{
                                pathname: '/',
                                state: {originPath: props.match.path}
                            }}
                        />
                    );
                }

            } else {
                return (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: {originPath: props.match.path}
                        }}
                    />
                );
            }
        }}
    />
);

const mapStateToProps = state => ({
    auth: state.auth,
});

export default connect(mapStateToProps)(GroupRoute);