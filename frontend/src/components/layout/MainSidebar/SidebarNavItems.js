import React from "react";
import {Nav} from "shards-react";

import SidebarNavItem from "./SidebarNavItem";
import {Store} from "../../../flux";
import {connect} from "react-redux";

class SidebarNavItems extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            navItems: Store.getSidebarItems()
        };

        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        Store.addChangeListener(this.onChange);
    }

    componentWillUnmount() {
        Store.removeChangeListener(this.onChange);
    }

    onChange() {
        this.setState({
            ...this.state,
            navItems: Store.getSidebarItems()
        });
    }

    render() {
        const {navItems: items} = this.state;
        const groups = this.props.user.groups;

        return (
            <div className="nav-wrapper">
                <Nav className="nav--no-borders flex-column">
                    {items.map((item, idx) => {
                        let has_group = false
                        let intersections = item.groups.filter(e => groups.indexOf(e) !== -1);
                        console.log(intersections);
                        if (item.groups.length === 0 || intersections.length > 0) {
                            has_group = true;
                        }
                        return (
                            has_group ? <SidebarNavItem key={idx} item={item}/> : null
                        )
                    })}

                </Nav>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        user: state.auth.user,
    }
};

const mapDispatchToProps = dispatch => {
    return {}
};


export default connect(mapStateToProps, mapDispatchToProps)(SidebarNavItems);