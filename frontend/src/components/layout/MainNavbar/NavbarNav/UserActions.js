import React from "react";
import {Collapse, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink} from "shards-react";

import {auth} from "../../../../actions";
import {connect} from "react-redux";

class UserActions extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false
        };

        this.toggleUserActions = this.toggleUserActions.bind(this);
    }

    toggleUserActions() {
        this.setState({
            visible: !this.state.visible
        });
    }

    render() {
        return (
            <NavItem tag={Dropdown} caret toggle={this.toggleUserActions}>
                <DropdownToggle caret tag={NavLink} className="text-nowrap px-3 user-menu">
                    <span className="d-md-inline-block user-menu">{this.props.user.username}</span>
                </DropdownToggle>
                <Collapse tag={DropdownMenu} right small open={this.state.visible}>
                    {/*<DropdownItem tag={Link} to="user-profile">*/}
                    {/*  <i className="material-icons">&#xE7FD;</i> Profile*/}
                    {/*</DropdownItem>*/}
                    {/*<DropdownItem tag={Link} to="edit-user-profile">*/}
                    {/*  <i className="material-icons">&#xE8B8;</i> Edit Profile*/}
                    {/*</DropdownItem>*/}
                    {/*<DropdownItem tag={Link} to="file-manager-list">*/}
                    {/*  <i className="material-icons">&#xE2C7;</i> Files*/}
                    {/*</DropdownItem>*/}
                    {/*<DropdownItem tag={Link} to="transaction-history">*/}
                    {/*  <i className="material-icons">&#xE896;</i> Transactions*/}
                    {/*</DropdownItem>*/}
                    {/*<DropdownItem divider />*/}
                    <DropdownItem className="text-danger" onClick={this.props.logout}>
                        <i className="material-icons text-danger">&#xE879;</i> Logout
                    </DropdownItem>
                </Collapse>
            </NavItem>
        );
    }
}


const mapStateToProps = state => {
    return {
        user: state.auth.user,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(auth.logout()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserActions)