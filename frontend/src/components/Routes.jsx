import React, {Component} from "react";
import {connect} from "react-redux";

import {BrowserRouter as Router} from "react-router-dom";
import routes from "../routes";
import withTracker from "../withTracker";


class Routes extends Component {

    render_admin = (admin_component, admin_user) => {

    };

    render() {
        return (
            <Router>
                <div>
                    {routes.map((route, index) => {
                        return (
                            <route.routeComponent
                                key={index}
                                path={route.path}
                                exact={route.exact}
                                groups={route.groups}
                                component={withTracker(props => {
                                    return (
                                        <route.layout {...props}>
                                            <route.component {...props} />
                                        </route.layout>
                                    );
                                })}
                            />
                        );
                    })}
                </div>
            </Router>
        )
    }
}

const mapStateToProps = state => {

    return {
        // user: state.auth.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
