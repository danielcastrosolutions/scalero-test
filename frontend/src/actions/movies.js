import axios from 'axios';


export const fetch_movies = () => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        let {token} = getState().auth;

        if (token) {
            headers["Authorization"] = `Token ${token}`;
        }

        return axios.get("/api/v1/movies/", {headers}).then(res => {
            return dispatch({type: 'FETCH_MOVIES', movies: res.data})
        }).catch(err => {
            console.log("Error fetching movies");
            console.log(err);
            console.log(err.response.status);
        })
    }
};

export const fetch_movie = (id) => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        let {token} = getState().auth;

        if (token) {
            headers["Authorization"] = `Token ${token}`;
        }

        return axios.get(`/api/v1/movies/${id}/`, {headers}).then(res => {
            return dispatch({type: 'FETCH_MOVIE', movie: res.data})
        }).catch(err => {
            console.log("Error fetching movie");
            console.log(err);
            console.log(err.response.status);
        })
    }
};

export const update_movie = (id, data) => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        let {token} = getState().auth;

        if (token) {
            headers["Authorization"] = `Token ${token}`;
        }

        return axios.patch(`/api/v1/movies/${id}/`, data, {headers}).then(res => {
            return dispatch({type: 'UPDATE_MOVIE', movie: res.data})
        }).catch(err => {
            console.log("Error fetching movie");
            console.log(err);
            console.log(err.response.status);
        })
    }
};

export const create_movie = (data) => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        let {token} = getState().auth;

        if (token) {
            headers["Authorization"] = `Token ${token}`;
        }

        console.log('data', data);

        return axios.post("/api/v1/movies/", data,{headers}).then(res => {
            return dispatch({type: 'CREATE_MOVIE', movie: res.data})
        }).catch(err => {
            console.log("Error creating movie");
            console.log(err);
            console.log(err.response.status);
        })
    }
};

export const create_review = (data) => {
    return (dispatch, getState) => {
        let headers = {"Content-Type": "application/json"};
        let {token} = getState().auth;

        if (token) {
            headers["Authorization"] = `Token ${token}`;
        }

        console.log('data', data);

        return axios.post("/api/v1/reviews/", data,{headers}).then(res => {
            return dispatch({type: 'CREATE_REVIEW', review: res.data})
        }).catch(err => {
            console.log("Error creating review");
            console.log(err);
            console.log(err.response.status);
        })
    }
};
