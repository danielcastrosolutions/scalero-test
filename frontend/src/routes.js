import React from "react";
import {Redirect, Route} from "react-router-dom";

import PrivateRoute from "./components/common/PrivateRoute";

// Layout Types
import {DefaultLayout} from "./layouts";

import LoginLayout from "./layouts/Login";
import Login from "./views/Login";
import Home from "./views/Home";
import Movies from "./views/Movies";
import MovieDetail from "./views/MovieDetail";

export default [
    {
        path: "/",
        exact: true,
        layout: DefaultLayout,
        component: () => <Redirect to="/movies"/>,
        routeComponent: PrivateRoute,
        groups: [""],
    },
    {
        path: "/login",
        exact: true,
        layout: LoginLayout,
        component: Login,
        routeComponent: Route,
        groups: [""],
    },
    {
        path: "/home",
        layout: DefaultLayout,
        component: Home,
        routeComponent: PrivateRoute,
        groups: [""],
    },
    {
        path: "/movies",
        exact: true,
        layout: DefaultLayout,
        component: Movies,
        routeComponent: PrivateRoute,
        groups: [""],
    },
    {
        path: "/movies/:id",
        exact: true,
        layout: DefaultLayout,
        component: MovieDetail,
        routeComponent: PrivateRoute,
        groups: [""],
    },
];
