import React, {Component} from "react";
import {
    Button,
    Card,
    CardBody,
    CardHeader,
    Col,
    Container,
    Form,
    FormCheckbox,
    FormInput,
    FormSelect,
    FormTextarea,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from "shards-react";

import {connect} from "react-redux";

import {movies} from "../actions";


class MovieDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            description: "",
            year: "",
            edit: false,
            modal: false,
            review_text: "",
            review_state: "",
        }

        this.update_movie = this.update_movie.bind(this);
        this.toggle_modal = this.toggle_modal.bind(this);
        this.submit_review = this.submit_review.bind(this);
    }

    update_movie(event) {
        event.preventDefault();
        let data = {
            name: this.state.name,
            description: this.state.description,
            year: this.state.year,
        }
        this.props.update_movie(this.props.match.params.id, data).then(() => {
            this.setState({edit: false})
        });
    }

    submit_review(event) {
        event.preventDefault();
        console.log('SUBMIT REVIEW');
        let data = {
            content: this.state.review_text,
            positive: this.state.review_state,
            movie: this.props.match.params.id,
        }
        this.props.create_review(data).then(() => {
            this.setState({
                modal: false,
                review_text: "",
                review_state: "",
            })
        });
    }

    toggle_modal = () => {
        this.setState({
            modal: !this.state.modal
        });
    };

    componentDidMount() {
        this.props.fetch_movie(this.props.match.params.id).then(() => {
            const {movie} = this.props
            this.setState({
                name: movie.name,
                description: movie.description,
                year: movie.year,
            })
        })
    }

    render() {
        const {movie} = this.props

        return <><Container fluid className="main-content-container px-4">
            <Row>

                <Col>
                    <Card className="my-3">
                        <CardHeader className="border-bottom">

                            <h6 className="m-0">Movie Details <i className="material-icons add-elements"
                                                                 onClick={(e) => this.setState({edit: !this.state.edit})}>
                                edit</i>
                            </h6>
                        </CardHeader>
                        <CardBody>

                            <Form onSubmit={this.update_movie}>
                                <Row>
                                    <Col>

                                        <Row form>
                                            <Col md="6" className="form-group">
                                                <label htmlFor="name">Movie Name</label>
                                                <FormInput
                                                    id="name"
                                                    type="text"
                                                    value={this.state.name}
                                                    onChange={(e) => this.setState({name: e.target.value})}
                                                    required
                                                    disabled={!this.state.edit}
                                                />
                                            </Col>
                                            <Col md="6" className="form-group">
                                                <label htmlFor="year">Year</label>
                                                <FormInput
                                                    id="year"
                                                    type="number"
                                                    value={this.state.year}
                                                    onChange={(e) => this.setState({year: e.target.value})}
                                                    required
                                                    disabled={!this.state.edit}
                                                />
                                            </Col>

                                            <Col lg="12" className="form-group">
                                                <label htmlFor="description">Description</label>
                                                <FormTextarea
                                                    id="description"
                                                    value={this.state.description}
                                                    onChange={(e) => this.setState({description: e.target.value})}
                                                    disabled={!this.state.edit}
                                                >
                                                </FormTextarea>
                                            </Col>
                                        </Row>
                                        {this.state.edit &&
                                        <Row>
                                            <Col className="my-4">
                                                <Button type="submit" className="col-lg-12">Save</Button>
                                            </Col>
                                        </Row>
                                        }
                                    </Col>
                                </Row>
                            </Form>
                            <Row>
                                <Col md={6}>
                                    <table className="table mb-0 my-4 borderless">
                                        <tbody>

                                        <tr>
                                            <td>Likes</td>
                                            <td>{movie.likes}</td>
                                        </tr>
                                        <tr>

                                            <td>Dislikes</td>
                                            <td>{movie.dislikes}</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

            <Row>
                <Col>
                    <Card className="my-3">
                        <CardHeader className="border-bottom">

                            <h6 className="m-0">Reviews <i className="material-icons add-elements"
                                                           onClick={(e) => this.setState({modal: !this.state.modal})}>
                                add</i></h6>
                        </CardHeader>
                        <CardBody>
                            <div className="table-responsive">
                                <table className="table mb-0">
                                    <thead className="bg-light">
                                    <tr>
                                        <th scope="col" className="border-0">
                                            Reviewer
                                        </th>
                                        <th scope="col" className="border-0">
                                            Reviewed at
                                        </th>
                                        <th scope="col" className="border-0">
                                            Positive
                                        </th>
                                        <th scope="col" className="border-0">
                                            Content
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {movie.reviews.map(review =>
                                        <tr key={review.id}>
                                            <td>{review.created_by}</td>
                                            <td>{review.created_at}</td>
                                            <td><FormCheckbox checked={review.positive}/></td>
                                            <td>{review.content}</td>
                                        </tr>
                                    )}
                                    </tbody>
                                </table>
                            </div>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

            <Modal open={this.state.modal} toggle={this.toggle_modal} size="lg">
                <ModalHeader>Create Review</ModalHeader>
                <ModalBody>
                    <Form onSubmit={this.submit_review}>
                        <Row>
                            <Col lg="12" className="form-group">
                                <label htmlFor="Consumption">Did you like the movie</label>
                                <FormSelect
                                    value={this.state.review_state}
                                    onChange={(e) => this.setState({review_state: e.target.value})}
                                    required
                                >
                                    <option value="">---</option>
                                    <option value="true">Yes</option>
                                    <option value="false">No</option>
                                </FormSelect>
                            </Col>
                            <Col lg="12">
                                <label htmlFor="Consumption">Review</label>
                                <FormTextarea
                                    type="text"
                                    value={this.state.review_text}
                                    onChange={(e) => this.setState({review_text: e.target.value})}
                                    required
                                />
                            </Col>
                            <Col lg="12" className="my-4">
                                <Button type="submit" className="col-lg-12">Create Review</Button>
                            </Col>
                        </Row>
                    </Form>
                </ModalBody>
            </Modal>

        </Container></>
            ;
    }

}

const mapStateToProps = state => {
        return {
            movie: state.movies.movie,
        }
    }
;

const mapDispatchToProps = dispatch => {
        return {
            fetch_movie: (id) => {
                return dispatch(movies.fetch_movie(id))
            },
            update_movie: (id, data) => {
                return dispatch(movies.update_movie(id, data))
            },
            create_review: (data) => {
                return dispatch(movies.create_review(data))
            },
        }
    }
;


export default connect(mapStateToProps, mapDispatchToProps)(MovieDetail);
