import React, {Component} from "react";
import {Card, CardBody, CardHeader, Col, Container, Row, Button, FormInput, FormTextarea, Form} from "shards-react";

import {connect} from "react-redux";

import {movies} from "../actions";
import {Link} from "react-router-dom";


class Movies extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            description: "",
            year: "",
        }

        this.submit_movie = this.submit_movie.bind(this);
    }

    componentDidMount() {
        // console.log("component did mount");
        this.props.fetch_movies();
    }

    submit_movie(event) {
        event.preventDefault();
        console.log('SUBMIT')
        this.props.create_movie(this.state);
    }

    render() {
        const {movies} = this.props

        return <><Container fluid className="main-content-container px-4">

            <Row>

                <Col>
                    <Card className="my-3">
                        <CardHeader>
                            <h6 className="m-0">Movie List</h6>
                        </CardHeader>

                        <CardBody>
                            <div className="table-responsive">
                                <table className="table mb-0">
                                    <thead className="bg-light">
                                    <tr>
                                        <th scope="col" className="border-0">
                                            Name
                                        </th>
                                        <th scope="col" className="border-0">
                                            Year
                                        </th>
                                        <th scope="col" className="border-0">
                                            Created By
                                        </th>
                                        <th scope="col" className="border-0">
                                            View
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {movies.map(movie =>
                                        <tr key={movie.id}>
                                            {/*<td>{movie.id}</td>*/}
                                            <td>{movie.name}</td>
                                            <td>{movie.year}</td>
                                            <td>{movie.created_by}</td>
                                            <td><Link to={`/movies/${movie.id}`}><Button
                                                className="m-0">View</Button></Link></td>
                                        </tr>
                                    )}
                                    </tbody>
                                </table>
                            </div>
                        </CardBody>
                    </Card>
                </Col>

                <Col>
                    <Card className="my-3">
                        <CardHeader>
                            <h6 className="m-0">Add Movie</h6>
                        </CardHeader>

                        <CardBody>
                            <Form onSubmit={this.submit_movie}>
                                <Row>
                                    <Col>
                                        <Row form>
                                            <Col md="6" className="form-group">
                                                <label htmlFor="name">Movie Name</label>
                                                <FormInput
                                                    id="name"
                                                    type="text"
                                                    value={this.state.name}
                                                    onChange={(e) => this.setState({name: e.target.value})}
                                                    required
                                                    className="upper"
                                                />
                                            </Col>
                                            <Col md="6" className="form-group">
                                                <label htmlFor="year">Year</label>
                                                <FormInput
                                                    id="year"
                                                    type="number"
                                                    value={this.state.year}
                                                    onChange={(e) => this.setState({year: e.target.value})}
                                                    required
                                                    className="upper"
                                                />
                                            </Col>

                                            <Col lg="12" className="form-group">
                                                <label htmlFor="description">Description</label>
                                                <FormTextarea
                                                    id="description"
                                                    value={this.state.description}
                                                    onChange={(e) => this.setState({description: e.target.value})}
                                                >
                                                </FormTextarea>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col className="my-4">
                                                <Button type="submit" className="col-lg-12">Create</Button>
                                            </Col>
                                        </Row>

                                    </Col>
                                </Row>
                            </Form>
                        </CardBody>
                    </Card>
                </Col>

            </Row>
        </Container></>;
    }

}

const mapStateToProps = state => {
    return {
        movies: state.movies.movies,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetch_movies: () => {
            return dispatch(movies.fetch_movies())
        },
        create_movie: (data) => {
            return dispatch(movies.create_movie(data))
        },
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Movies);
