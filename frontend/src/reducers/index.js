import { combineReducers } from 'redux';

import auth from "./auth";
import movies from "./movies";

const projectApp = combineReducers({
    auth, movies,
});

export default projectApp;