const initialState = {
    movies: [],
    movie: {reviews: []}
};

export default function movies(state = initialState, action) {
    switch (action.type) {

        case 'FETCH_MOVIES':
            return {...state, movies: action.movies};

        case 'FETCH_MOVIE':
            return {...state, movie: action.movie}

        case 'CREATE_MOVIE':
            return {...state, movies: [action.movie, ...state.movies]}

        case 'CREATE_REVIEW':
            return {...state, movie: {...state.movie, reviews: [action.review, ...state.movie.reviews]}}
        default:
            return state;
    }
}